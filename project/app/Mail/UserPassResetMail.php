<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserPassResetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $password;
    public $subject;
    public $template;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($password, $subject, $template)
    {
        $this->password = $password;
        $this->subject = $subject;
        $this->template = $template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
            ->view('emails.registration.reset-password');
    }
}
