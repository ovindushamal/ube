<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\UserRegistrationMail;
use App\Models\EmailSubject;
use App\Models\EmailTemplate;
use App\ReferralProgram;
use App\UserProfile;
use Illuminate\Foundation\Auth\ResetsPasswords;
use DB;
use Mail;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function userActivation($token)
    {

        $check = DB::table('user_activations')->where('token', $token)->first();

        if (!is_null($check)) {
            $user = UserProfile::find($check->id_user);
            if ($user->is_activated == 1) {
                return redirect()->to('signin/user')->with('success', "user is already actived.");
            }

            $user->update(['is_activated' => 1]);
            DB::table('user_activations')->where('token', $token)->delete();

            // user email
            $EmailSubject = EmailSubject::where('token', 'fIfX3uR9')->first();

            $EmailTemplate = EmailTemplate::where('domain', 1)->where('subject_id', $EmailSubject['id'])->first();
            $signUpBonus = ReferralProgram::where('name', '=', 'Sign-up Bonus')->first()->amount;
            $gift_card_value = 25;
            Mail::to($user->email)->queue(new UserRegistrationMail($user->first_name, $EmailSubject['subject'], $EmailTemplate, $gift_card_value, $signUpBonus));
            return redirect()->to('signin/user')->with('success', "user active successfully.");
        } else {

            return redirect()->to('signin/user')->with('warning', "your token is invalid");
        }
    }
}
