<?php

namespace App\Http\Controllers;

use App\Mail\AdminStatusUpdate;
use App\Mail\AdminStatusUpdateCustom;
use App\Mail\OrderComplete;
use App\Models\EmailSubject;
use App\Models\EmailTemplate;
use App\Order;
use App\OrderedProducts;
use App\Product;
use App\UserProfile;
use App\Vendors;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Order::$withoutAppends = true;

//        $email_templates =  EmailTemplate:: join('email_subjects','email_contents.subject_id','=','email_subjects.id')
//            ->select('*','email_contents.id AS template_id')
//            ->whereIn('email_subjects.token',['v3G8DXbW', 'N5MmiaEb', '72HJYFzB'])
//            ->get();

        $order_status_array = array(0 =>'Scheduled',1 =>'In Transit',2 =>'At Plant',3 =>'On Delivery',4 =>'At_Plant Completed',5 =>'Completed',6 =>'Completed At Store');
        $order_status_mails = array();
        foreach ($order_status_array as $row)
        {
            if($row == 'In Transit')
            {
                $email_templates =  EmailTemplate:: join('email_subjects','email_contents.subject_id','=','email_subjects.id')
                    ->where('email_subjects.token','=','N5MmiaEb')
                    ->first();
                array_push($order_status_mails,[
                    'status' => $row,
                    'email_body' => $email_templates->content,
                    'email_subject' => $email_templates->subject,
                ]);
            }
            elseif($row == 'On Delivery')
            {
                $email_templates =  EmailTemplate:: join('email_subjects','email_contents.subject_id','=','email_subjects.id')
                    ->where('email_subjects.token','=','72HJYFzB')
                    ->first();
                array_push($order_status_mails,[
                    'status' => $row,
                    'email_body' => $email_templates->content,
                    'email_subject' => $email_templates->subject,
                ]);
            }
            elseif ($row == 'Completed')
            {
                $email_templates =  EmailTemplate:: join('email_subjects','email_contents.subject_id','=','email_subjects.id')
                    ->where('email_subjects.token','=','v3G8DXbW')
                    ->first();
                array_push($order_status_mails,[
                    'status' => $row,
                    'email_body' => $email_templates->content,
                    'email_subject' => $email_templates->subject,
                ]);
            }
            else
            {
                array_push($order_status_mails,[
                    'status' => $row,
                    'email_body' => null,
                    'email_subject' => null,
                ]);
            }
        }

//        return response()->json([$order_status_mails]);

        $orders = Order::where('payment_status',"Completed")->orderBy('id','desc')->get();
        return view('admin.orderlist',compact('orders'), compact('order_status_mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::findOrFail($id);
        $products = OrderedProducts::where('orderid',$id)->get();
        return view('admin.orderdetails',compact('order','products'));
    }

    public function sendBulkEmail(Request $request)
    {
        $template = array();
        $order_status = $request->template;
        $order_ids = $request->email_multi_select;
        $order_ids = str_replace("undefined,", "", $order_ids);
        $order_ids = str_replace("undefined", "", $order_ids);
        $mail_subject =  $request->email_subject;
        $mail_body = $request->email_body;
        array_push($template,[
            'content' => $mail_body
        ]);
        $mail_subject = explode(",", $mail_subject);
        $orders = Order::whereIn('id',$order_ids)
            ->get();
        foreach ($orders as $payee)
        {
            if(isset($payee->customer_email) && $payee->customer_email != null)
            {
                try
                {
                    $stat['status'] = strtolower($order_status);
                    $mainorder = Order::findOrFail($payee->id);
                    // return $mainorder->status;
                    if($mainorder->status != "completed")
                    {
                        $user = UserProfile::where('id', $mainorder['customerid'])->first();
                        if($order_status == 'In Transit')
                        {
                            $orders2 = OrderedProducts::where('orderid',$payee->id)->get();

                            foreach ($orders2 as $payee2) {
                                $order = OrderedProducts::findOrFail($payee2->id);
                                $sts['status'] = "in_transit";
                                $order->update($sts);
                            }

                            Mail::to($payee->customer_email)->send(new AdminStatusUpdate($user->first_name,$mail_subject[0],$template[0],$order_status));

                        }
                        elseif ($order_status == 'On Delivery')
                        {
                            $orders2 = OrderedProducts::where('orderid',$payee->id)->get();

                            foreach ($orders2 as $payee2) {
                                $order = OrderedProducts::findOrFail($payee2->id);
                                $sts['status'] = "on_delivery";
                                $order->update($sts);
                            }
                            Mail::to($payee->customer_email)->send(new AdminStatusUpdate($user->first_name,$mail_subject[0],$template[0],$order_status));
                        }
                        elseif($order_status == 'Completed')
                        {
                            $orders2 = OrderedProducts::where('orderid',$payee->id)->get();

                            foreach ($orders2 as $payee2) {
                                $order = OrderedProducts::findOrFail($payee2->id);

                                if ($order->owner == "vendor"){
                                    $vendor = Vendors::findOrFail($payee2->vendorid);
                                    $balance['current_balance'] = $vendor->current_balance + $payee2->cost;
                                    $vendor->update($balance);
                                }
                                $sts['paid'] = "yes";
                                $sts['status'] = "completed";
                                $order->update($sts);
                            }

                            Mail::to($payee->customer_email)->send(new OrderComplete($user->first_name, $mail_subject[0],$template[0]));
                        }
                        elseif ($order_status == 'Scheduled')
                        {
                            $orders2 = OrderedProducts::where('orderid',$payee->id)->get();

                            foreach ($orders2 as $payee2) {
                                $order = OrderedProducts::findOrFail($payee2->id);
                                $sts['status'] = "scheduled";
                                $order->update($sts);
                            }

                            Mail::to($payee->customer_email)->send(new AdminStatusUpdateCustom($mail_subject[0],$mail_body));
                        }
                        elseif ($order_status == 'At Plant')
                        {
                            $orders2 = OrderedProducts::where('orderid',$payee->id)->get();

                            foreach ($orders2 as $payee2) {
                                $order = OrderedProducts::findOrFail($payee2->id);
                                $sts['status'] = "at_plant";
                                $order->update($sts);
                            }

                            Mail::to($payee->customer_email)->send(new AdminStatusUpdateCustom($mail_subject[0],$mail_body));

                        }
                        elseif ($order_status = 'At Plant Completed')
                        {
                            $orders2 = OrderedProducts::where('orderid',$payee->id)->get();

                            foreach ($orders2 as $payee2) {
                                $order = OrderedProducts::findOrFail($payee2->id);
                                $sts['status'] = "at_plant_completed";
                                $order->update($sts);
                            }

                            Mail::to($payee->customer_email)->send(new AdminStatusUpdateCustom($mail_subject[0],$mail_body));

                        }
                        elseif ($order_status == 'Completed At Store')
                        {
                            $orders2 = OrderedProducts::where('orderid',$payee->id)->get();

                            foreach ($orders2 as $payee2) {
                                $order = OrderedProducts::findOrFail($payee2->id);
                                $sts['status'] = "completed_at_store";
                                $order->update($sts);
                            }

                            Mail::to($payee->customer_email)->send(new AdminStatusUpdateCustom($mail_subject[0],$mail_body));

                        }
                        $mainorder->update($stat);
                        sleep(5);
                    }

                }
                catch (Exception $e)
                {
                    return redirect('admin/orders');
                }
            }
        }
        return redirect('admin/orders');
    }

    public function status($id,$status)
    {


        $stat['status'] = $status;
        $mainorder = Order::findOrFail($id);
        $user = UserProfile::where('id', $mainorder['customerid'])->first();

        if ($mainorder->status == "completed"){
            return redirect('admin/orders')->with('message','This Order is Already Completed');
        }else{


            if ($status == "completed"){
                $orders = OrderedProducts::where('orderid',$id)->get();

                foreach ($orders as $payee) {
                    $order = OrderedProducts::findOrFail($payee->id);

                    if ($order->owner == "vendor"){
                        $vendor = Vendors::findOrFail($payee->vendorid);
                        $balance['current_balance'] = $vendor->current_balance + $payee->cost;
                        $vendor->update($balance);
                    }
                    $sts['paid'] = "yes";
                    $sts['status'] = "completed";
                    $order->update($sts);
                }
                //user mail
                $EmailSubject = EmailSubject::where('token', 'v3G8DXbW')->first();
                $EmailTemplate = EmailTemplate::where('domain', 1)->where('subject_id', $EmailSubject['id'])->first();
                Mail::to($user->email)->send(new OrderComplete($user->first_name,$EmailSubject['subject'],$EmailTemplate));
            }
            if ($status == "in transit"){

                $orders = OrderedProducts::where('orderid',$id)->get();

                foreach ($orders as $payee) {
                    $order = OrderedProducts::findOrFail($payee->id);
                    $sts['status'] = "in_transit";
                    $order->update($sts);
                }
                $EmailSubject = EmailSubject::where('token', 'N5MmiaEb')->first();
                $EmailTemplate = EmailTemplate::where('domain', 1)->where('subject_id', $EmailSubject['id'])->first();
                Mail::to($user->email)->send(new AdminStatusUpdate($user->first_name,$EmailSubject['subject'],$EmailTemplate,$status));
            }
            if ($status == "at plant"){

                $orders = OrderedProducts::where('orderid',$id)->get();

                foreach ($orders as $payee) {
                    $order = OrderedProducts::findOrFail($payee->id);
                    $sts['status'] = "at_plant";
                    $order->update($sts);
                }
            }
            if ($status == "at plant completed"){

                $orders = OrderedProducts::where('orderid',$id)->get();

                foreach ($orders as $payee) {
                    $order = OrderedProducts::findOrFail($payee->id);
                    $sts['status'] = "at_plant_completed";
                    $order->update($sts);
                }
            }
            if ($status == "on delivery"){

                $orders = OrderedProducts::where('orderid',$id)->get();

                foreach ($orders as $payee) {
                    $order = OrderedProducts::findOrFail($payee->id);
                    $sts['status'] = "on_delivery";
                    $order->update($sts);
                }
                //user mail
                $EmailSubject = EmailSubject::where('token', '72HJYFzB')->first();
                $EmailTemplate = EmailTemplate::where('domain', 1)->where('subject_id', $EmailSubject['id'])->first();
                Mail::to($user->email)->send(new AdminStatusUpdate($user->first_name,$EmailSubject['subject'],$EmailTemplate,$status));
            }
            if ($status == "completed at store"){

                $orders = OrderedProducts::where('orderid',$id)->get();

                foreach ($orders as $payee) {
                    $order = OrderedProducts::findOrFail($payee->id);
                    $sts['status'] = "completed_at_store";
                    $order->update($sts);
                }
            }
            if ($status == "scheduled"){

                $orders = OrderedProducts::where('orderid',$id)->get();

                foreach ($orders as $payee) {
                    $order = OrderedProducts::findOrFail($payee->id);
                    $sts['status'] = "scheduled";
                    $order->update($sts);
                }
            }


        }
        //return $stat;
        $mainorder->update($stat);


        return redirect('admin/orders')->with('message','Order Status Updated Successfully');
    }

    public function email($id)
    {
        $order = Order::findOrFail($id);
        return view('admin.sendmail', compact('order'));
    }

    public function sendemail(Request $request)
    {
        mail($request->to,$request->subject,$request->message);
        return redirect('admin/orders')->with('message','Email Send Successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
